# frankenstein Quickstart

Please see **dna** QuickStart which describes how this application was created.

- [dna Quickstart](//docs.elioway/./eliothing/dna/doc/quickstart)

## Nutshell

```
cd frankenstein
virtualenv --python=python3 venv-frankenstein
pip install -e .
./init_frankenstein.sh
```
