#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e

export DJANGO_SETTINGS_MODULE="frankenstein.settings"

# py.test -x
touch frankenstein.db # && rm frankenstein.db
django-admin makemigrations frankenstein
django-admin migrate --noinput --database=default
django-admin collectstatic --noinput
django-admin runserver 0.0.0.0:8000
