![](https://elioway.gitlab.io/eliofaithful/frankenstein/elio-frankenstein-logo.png)

> It lives **the elioWay**

# frankenstein ![experimental](/artwork/icon/experimental/favicon.png "experimental")

A new creature built **the elioWay**. This form the basis of a tutorial.

- [frankenstein Documentation](https://elioway.gitlab.io/eliofaithful/frankenstein)
- [dna Documentation](https://elioway.gitlab.io/eliothing/dna)

## Installing

```
git clone https://gitlab.com/eliofaithful/frankenstein.git my_monster_app
```

- [Installing frankenstein](https://elioway.gitlab.io/eliofaithful/frankenstein/installing.html)

## Seeing is believing

```bash
virtualenv --python=python3 venv-frankenstein
source venv-frankenstein/bin/activate.fish
pip install -e .
./init_frankenstein.sh
```

## Nutshell

```
django-admin runserver 0.0.0.0:8000
```

- [frankenstein Quickstart](https://elioway.gitlab.io/eliofaithful/frankenstein/quickstart.html)
- [frankenstein Credits](https://elioway.gitlab.io/eliofaithful/frankenstein/credits.html)

![](https://elioway.gitlab.io/eliofaithful/frankenstein/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
