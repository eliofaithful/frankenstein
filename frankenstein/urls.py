# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from frankenstein.views import (
    FrankensteinCreateView,
    FrankensteinUpdateView,
    FrankensteinDeleteView,
    FrankensteinEngageView,
    FrankensteinListView,
    FrankensteinIterateView,
    FrankensteinOptimizeView,
    FrankensteinPageView,
    FrankensteinHomeView,
)

urlpatterns = [
    path(
        "cr/<str:page_slug>/<str:role>/<str:thing>",
        FrankensteinCreateView.as_view(),
        name="create.thing",
    ),
    path(
        "u/<str:page_slug>/<str:role>/<str:thing>/<int:pk>",
        FrankensteinUpdateView.as_view(),
        name="update.thing",
    ),
    path(
        "d/<str:page_slug>/<str:thing>/<int:pk>",
        FrankensteinDeleteView.as_view(),
        name="delete.thing",
    ),
    path(
        "e/<str:page_slug>/<str:thing>/<int:pk>",
        FrankensteinEngageView.as_view(),
        name="engage.thing",
    ),
    path(
        "l/<str:page_slug>/<str:thing>",
        FrankensteinListView.as_view(),
        name="list.things",
    ),
    path(
        "i/<str:page_slug>/<str:thing>/<str:field>",
        FrankensteinIterateView.as_view(),
        name="iterate.things",
    ),
    path(
        "o/<str:page_slug>/<str:thing>",
        FrankensteinOptimizeView.as_view(),
        name="optimize.things",
    ),
    path(
        "p/<str:page_slug>", FrankensteinPageView.as_view(), name="engage.page"
    ),
    path("", FrankensteinHomeView.as_view(), name="home"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
