# -*- encoding: utf-8 -*-
from dna.views import (
    EveryCreateViewMixin,
    EveryUpdateViewMixin,
    EveryDeleteViewMixin,
    EveryEngageViewMixin,
    EveryListViewMixin,
    EveryIterateViewMixin,
    EveryOptimizeViewMixin,
    EveryPageViewMixin,
    EveryHomeViewMixin,
)


class FrankensteinCreateView(EveryCreateViewMixin):
    template_name = "frankenstein/thing_form.html"


class FrankensteinUpdateView(EveryUpdateViewMixin):
    template_name = "frankenstein/thing_form.html"


class FrankensteinDeleteView(EveryDeleteViewMixin):
    template_name = "frankenstein/thing_delete.html"


class FrankensteinEngageView(EveryEngageViewMixin):
    template_name = "frankenstein/thing_engaged.html"


class FrankensteinListView(EveryListViewMixin):
    template_name = "frankenstein/thing_listed.html"


class FrankensteinIterateView(EveryIterateViewMixin):
    template_name = "frankenstein/thing_iterate.html"


class FrankensteinOptimizeView(EveryOptimizeViewMixin):
    template_name = "frankenstein/thing_optimize.html"


class FrankensteinPageView(EveryPageViewMixin):
    template_name = "frankenstein/thing_page.html"


class FrankensteinHomeView(EveryHomeViewMixin):
    template_name = "frankenstein/home_page.html"
